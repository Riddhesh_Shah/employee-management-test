<?php

require_once('functions.inc.php');

if(!empty($_POST)){
  // $cities = db_select("Select * FROM cities where id = 1")[0];
  // dd($cities);
  $name = $_POST['name_input'];
  $email = $_POST['email_input'];
  $address_1 = $_POST['address_1_input'];
  $address_2 = $_POST['address_2_input'];
  $location = $_POST['location_input'];
  $pin_code = $_POST['pin_code_input'];
  $postal_area = $_POST['postal_area_input'];
  $taluka = $_POST['taluka_input'];
  $suburb = $_POST['suburb_input'];
  $east_west = $_POST['east_west_input'];
  $country = $_POST['country_select'];
  $state = $_POST['state_select'];
  $city = $_POST['city_select'];
  $district = $_POST['district_input'];
  $mobile = $_POST['mobile_input'];
  $whatsapp = $_POST['whatsapp_input'];

  // dd($name);
  // dd($city);

  $employee = ["name" => $name];
  db_insert("employee", $employee);

  $employee_last_inserted_id = db_select("SELECT id FROM employee ORDER BY id DESC LIMIT 1");

  $mobile_array = ["mobile_number" => $mobile, "employee_id" => $employee_last_inserted_id];
  db_insert("employee_mobile_number", $mobile_array);

  $whatsapp_array = ["whatsapp_number" => $whatsapp, "employee_id" => $employee_last_inserted_id];
  db_insert("employee_whatsapp_number", $whatsapp_array);

  $email_array = ["email" => $whatsapp, "employee_id" => $employee_last_inserted_id];
  db_insert("employee_whatsapp_number", $email_array);

  $address_array = ["address_1" => $address_1, "address_2" => $address_2, "location" => $location, "pin_code" => $pin_code, "postal_area" => $postal_area, "taluka" => $taluka, "suburb" => $suburb, "east_west" => $east_west, "country" => $country, "state" => $state, "city" => $city, "district" => $district];
  db_insert("address_employee", $address_array);

}

$countries = db_select("SELECT * FROM countries");
// dd($countries);
$states = db_select("SELECT * FROM states");
$cities = db_select("SELECT * FROM cities");

?>

<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    
        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
        <link rel="stylesheet" href="https://bootswatch.com/5/cerulean/bootstrap.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    
        <title>Hello, world!</title>
      </head>
    <!-- <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
        
    </head> -->
    <body>
        
        <div class="container mt-5 mb-5">

            <legend>Employee Details</legend>

            <form action="create_employee.php" method="POST">

              <div class="form-group mt-2 mb-2">
                <label class = "mb-2" for="name_input">Name</label>
                <input name = "name_input" type="text" class="form-control" id="name_input" aria-describedby="name_input" placeholder="Enter Name">
              </div>

              <div class="form-group mt-2 mb-2">
                <label class = "mb-2" for="email_input">Email address</label>
                <input name = "email_input" type="email" class="form-control" id="email_input" aria-describedby="email_input" placeholder="Enter email">
              </div>

              <div class="form-group mt-2 mb-2">
                <label class = "mb-2" for="address_1_input">Address 1</label>
                <input name = "address_1_input" type="text" class="form-control" id="address_1_input" aria-describedby="address_1_input" placeholder="Enter Address 1">
              </div>

              <div class="form-group mt-2 mb-2">
                <label class = "mb-2" for="address_2_input">Address 2</label>
                <input name = "address_2_input" type="text" class="form-control" id="address_2_input" aria-describedby="address_2_input" placeholder="Enter Address 2">
              </div>

              <div class="form-group mt-2 mb-2">
                <label class = "mb-2" for="location_input">Location</label>
                <input name = "location_input" type="text" class="form-control" id="location_input" aria-describedby="location_input" placeholder="Enter Location">
              </div>

              <div class="form-group mt-2 mb-2">
                <label class = "mb-2" for="pin_code_input">Pin Code</label>
                <input name = "pin_code_input" type="text" class="form-control" id="pin_code_input" aria-describedby="pin_code_input" placeholder="Enter Pincode">
              </div>

              <div class="form-group mt-2 mb-2">
                <label class = "mb-2" for="postal_area_input">Postal Area</label>
                <input name = "postal_area_input" type="text" class="form-control" id="postal_area_input" aria-describedby="postal_area_input" placeholder="Enter Postal Area">
              </div>

              <div class="form-group mt-2 mb-2">
                <label class = "mb-2" for="email_input">Taluka</label>
                <input name = "taluka_input" type="text" class="form-control" id="taluka_input" aria-describedby="taluka_input" placeholder="Enter Taluka">
              </div>

              <div class="form-group mt-2 mb-2">
                <label class = "mb-2" for="email_input">Suburb</label>
                <input name = "suburb_input" type="text" class="form-control" id="suburb_input" aria-describedby="suburb_input" placeholder="Enter email">
              </div>

              <div class="form-group mt-2 mb-2">
                <label class = "mb-2" for="email_input">East / West</label>
                <input name = "east_west_input" type="text" class="form-control" id="east_west_input" aria-describedby="east_west_input" placeholder="Enter East / West">
              </div>

              <div class="form-group mt-2 mb-2">
                <label for="country_select">Select Country</label>
                <select class="form-select" id="country_select" name = "country_select">
<?php
  foreach($countries as $country):
    // var_dump($country);
?>

                <option value="<?= $country["id"]?>"><?= $country["name"]?></option>

<?php
  endforeach
?>

                  
                </select>
              </div>

              <div class="form-group mt-2 mb-2">
                <label for="state_select">Select State</label>
                <select class="form-select" id="state_select" name = "state_select">
<?php
  foreach($states as $state):
    // var_dump($country);
?>

                <option value="<?= $state["id"]?>"><?= $state["name"]?></option>

<?php
  endforeach
?>
                </select>
              </div>
              
              <div class="form-group mt-2 mb-2">
                <label for="city_select">Select City</label>
                <select class="form-select" id="city_select" name = "city_select">
<?php
  foreach($cities as $city):
    // var_dump($country);
?>

                <option value="<?= $city["id"]?>"><?= $city["name"]?></option>

<?php
  endforeach
?>
                </select>
              </div>


              <div class="form-group mt-2 mb-2">
                <label class = "mb-2" for="district_input">District</label>
                <input name = "district_input" type="text" class="form-control" id="district_input" aria-describedby="district_input" placeholder="Enter District">
              </div>

              <div class="form-group mt-2 mb-2">
                <label class = "mb-2" for="mobile_input">Mobile Number</label>
                <input name = "mobile_input" type="text" class="form-control" id="mobile_input" aria-describedby="mobile_input" placeholder="Enter Mobile Number">
              </div>

              <div class="form-group mt-2 mb-2">
                <label class = "mb-2" for="whatsapp_input">Whatsapp Number</label>
                <input name = "whatsapp_input" type="text" class="form-control" id="whatsapp_input" aria-describedby="whatsapp_input" placeholder="Enter Whatsapp Number">
              </div>

              <!-- <div class="form-group mt-2 mb-2">
                <label class = "mb-2" for="state_input">State</label>
                <input name = "state_input" type="text" class="form-control" id="state_input" aria-describedby="state_input" placeholder="Enter State">
              </div>

              <div class="form-group mt-2 mb-2">
                <label class = "mb-2" for="country_input">Country</label>
                <input name = "country_input" type="text" class="form-control" id="ecountry_inputmail_input" aria-describedby="country_input" placeholder="Enter Country">
              </div> -->

              <button type="submit" class="btn btn-primary mt-2 mb-2">Submit</button>

            </form>

            <button onclick="window.location.href='display_employee.php'" class = "btn btn-secondary">Display Employees</button>


            <!-- <form>
                <fieldset>
                  <legend>Legend</legend>
                  <div class="form-group mt-2 mb-2 row">
                    <label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                      <input type="text" readonly="" class="form-control-plaintext" id="staticEmail" value="email@example.com">
                    </div>
                  </div>
                  <div class="form-group mt-2 mb-2">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                  </div>
                  <div class="form-group mt-2 mb-2">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                  </div>
                  <div class="form-group mt-2 mb-2">
                    <label for="exampleSelect1">Example select</label>
                    <select class="form-select" id="exampleSelect1">
                      <option>1</option>
                      <option>2</option>
                      <option>3</option>
                      <option>4</option>
                      <option>5</option>
                    </select>
                  </div>
                  <div class="form-group mt-2 mb-2">
                    <label for="exampleSelect2">Example multiple select</label>
                    <select multiple="" class="form-select" id="exampleSelect2">
                      <option>1</option>
                      <option>2</option>
                      <option>3</option>
                      <option>4</option>
                      <option>5</option>
                    </select>
                  </div>
                  <div class="form-group mt-2 mb-2">
                    <label for="exampleTextarea">Example textarea</label>
                    <textarea class="form-control" id="exampleTextarea" rows="3"></textarea>
                  </div>
                  <div class="form-group mt-2 mb-2">
                    <label for="formFile" class="form-label">Default file input example</label>
                    <input class="form-control" type="file" id="formFile">
                  </div>
                  <fieldset class="form-group mt-2 mb-2">
                    <legend>Radio buttons</legend>
                    <div class="form-check">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadios1" value="option1" checked="">
                        Option one is this and that—be sure to include why it's great
                      </label>
                    </div>
                    <div class="form-check">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadios2" value="option2">
                        Option two can be something else and selecting it will deselect option one
                      </label>
                    </div>
                    <div class="form-check disabled">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadios3" value="option3" disabled="">
                        Option three is disabled
                      </label>
                    </div>
                  </fieldset>
                  <fieldset class="form-group mt-2 mb-2">
                    <legend>Checkboxes</legend>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                      <label class="form-check-label" for="flexCheckDefault">
                        Default checkbox
                      </label>
                    </div>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" checked="">
                      <label class="form-check-label" for="flexCheckChecked">
                        Checked checkbox
                      </label>
                    </div>
                  </fieldset>
                  <fieldset>
                    <legend>Switches</legend>
                    <div class="form-check form-switch">
                      <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault">
                      <label class="form-check-label" for="flexSwitchCheckDefault">Default switch checkbox input</label>
                    </div>
                    <div class="form-check form-switch">
                      <input class="form-check-input" type="checkbox" id="flexSwitchCheckChecked" checked="">
                      <label class="form-check-label" for="flexSwitchCheckChecked">Checked switch checkbox input</label>
                    </div>
                  </fieldset>
                  <fieldset class="form-group mt-2 mb-2">
                    <legend>Ranges</legend>
                      <label for="customRange1" class="form-label">Example range</label>
                      <input type="range" class="form-range" id="customRange1">
                      <label for="disabledRange" class="form-label">Disabled range</label>
                      <input type="range" class="form-range" id="disabledRange" disabled="">
                      <label for="customRange3" class="form-label">Example range</label>
                      <input type="range" class="form-range" min="0" max="5" step="0.5" id="customRange3">
                  </fieldset>
                  <button type="submit" class="btn btn-primary">Submit</button>
                </fieldset>
              </form> -->
    
        </div>
        
    </body>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <script>
      var city = <?= dd_select("Select * from cities where state_id = 488"); ?>
      console.log(city);
    </script>
</html>