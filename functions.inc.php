<?php 

function db_connect(){
    static $connection;
    
    if(!$connection){
        //Try to Connect as database connection is not established
        $config = parse_ini_file('config.ini');
        
        $connection = mysqli_connect($config['host'], $config['username'], $config['password'], $config['dbname']);
    }
    
    //I assure that $connection has a valid connection established
    if($connection === false)
    {
        return mysqli_connect_error();
    }
    
    return $connection;
}

function db_query($query)
{
    $connection = db_connect();
    
    $result = mysqli_query($connection, $query);
    
    return $result;
}

function db_error()
{
    $connection = db_connect();
    return mysqli_error($connection);
}

function db_select($query)
{
    $result = db_query($query);
    if($result === false)
    {
        return false;    
    }$rows = array();
    while($row = mysqli_fetch_assoc($result))
    {
        $rows[] = $row;
    }
    
    return $rows;
}

function db_quote($value)
{
    $connection = db_connect();
    return mysqli_real_escape_string($connection, $value);
}

function dd($variable)
{
    die(var_dump($variable));
}

function add_single_quotes($variable)
{
    return "'$variable'";
}

function redirect($url)
{
    header("Location: $url");
}

function db_insert($table_name, $array){
    $keys = array_keys($array);
    $values = array_values($array);

    $keyString = $keys[0];
    $valueString = add_single_quotes($values[0]);
    for($i=1;$i<count($array);$i++):
        $keyString .= (", " . $keys[$i]);
        $valueString .= (", " . add_single_quotes($values[$i]));
    endfor; 

    $query = "INSERT INTO $table_name($keyString) VALUES ($valueString)";
    //dd($query);

    //dd($query);
    $result = db_query($query);
    //dd(db_error());

    if($result){
        return "Data Inserted Successfully";
    }else{
        return db_error();
    }

}

function db_update($table_name, $id, $array){
    $keys = array_keys($array);
    $values = array_values($array);

    $statment = ($keys[0] . " = " . add_single_quotes($values[0]));
    for($i=1;$i<count($array);$i++):
        $statment .= (", " . ($keys[$i] . " = " . add_single_quotes($values[$i])));
    endfor; 

    $query = "UPDATE $table_name $statment WHERE id = $id";

    $result = db_query($query);

    if($result){
        return "Data Updated Successfully";
    }else{
        return db_error();
    }

}

function db_delete($table_name, $id){

    $query = "DELETE FROM $table_name WHERE id = $id";

    $result = db_query($query);

    if($result){
        return "Data Updated Successfully";
    }else{
        return db_error();
    }

}

?>